from flask import Flask, Response  
import time

app = Flask(__name__)
 
@app.route("/index")
def hello():
    return "Hello World!"

@app.route("/time")
def web_clock_time():
    def eventStream():
        while True:
            yield clock_time() + '\n'
    return Response(eventStream(),status=200,mimetype="text/event-stream")
    #timenowt = time.ctime(time.time(), status=200,mimetype="text/html" )
    #return Response(timenowt, status=200, mimetype="text/html")

def clock_time():
    time.sleep(1.0)
    timenowt = time.ctime(time.time())
    return timenowt

def bootapp():
    app.run(port=8080, threaded=True, host=('0.0.0.0'))


if __name__ == "__main__":
        bootapp()

